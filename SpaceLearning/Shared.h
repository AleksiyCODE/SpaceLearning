#pragma once
#include <random>
constexpr float fieldWidth = 600.0f;
constexpr float fieldHeight = 400.0f;

enum class Team
{
	Player,
	Enemy
};

//static float GetRandomFloatNorm()
//{
//	return static_cast <float> (std::rand()) / static_cast <float> (RAND_MAX);
//}
//
//// [-1, 1]
//static float GetRandomFloatNegOneToOne()			
//{
//	return (static_cast <float> (std::rand()) / static_cast <float> (RAND_MAX)) * 2.0f - 1.0f;
//}
//