#include "Movable.h"

void Movable::ConfineInRect(olc::vf2d dimentions)
{
	if (position.x < 0)
	{
		position.x = 0;
		velocity.x = 0;
	}
	else
		if (position.x + size.x > dimentions.x)
		{
			position.x = float(dimentions.x - size.x );
			velocity.x = 0;
		}
	if (position.y < 0)
	{
		position.y = 0;
		velocity.y = 0;
	}
	else
		if (position.y + size.y > dimentions.y)
		{
			position.y = float(dimentions.y - size.y);
			velocity.y = 0;
		}
}
