#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"
#include "Game.h"
#include "GameSequencer.h"




class GraphicsModule : public olc::PixelGameEngine
{
public:
	GraphicsModule()
	{
		sAppName = "Space Learning";
		StaticKeyboard::SetEngine(this);
	}

public:


#define DRAWING
#define UPDATING

	bool OnUserUpdate(float fElapsedTime) override
	{
#ifdef UPDATING
		gs.StepPlayback(fElapsedTime);
		//game.Update(fElapsedTime);
#endif // UPDATING

#ifdef DRAWING
		Draw();
#endif // DRAWING		

		return true;
	}

#undef DRAWING
#undef UPDATING

private:
	bool OnUserCreate() override
	{
		//gs.SequenceTraining();
		gs.SequenceTraining("successResult.txt");

		//gs.BeginPlaybackNetwork(BrainType::Simple, std::string("WallMonger.txt"));
		gs.BeginPlaybackNetwork(BrainType::AFK, true);
		//gs.SequenceTesting();
		return true;
	}

	void Draw()
	{


		Clear(olc::BLACK);
		for (const auto& ship : gs.gg->ships)
		{			
			DrawRect(ship->GetPositionScaled(), ship->GetSizeScaled(), ship->GetColor());
			DrawString(olc::vf2d{ ship->GetPositionScaled().x, ship->GetPositionScaled().y}, std::to_string((int)ship->GetHealth()), ship->GetColor());
			DrawString(olc::vf2d{ ship->GetPositionScaled().x, ship->GetPositionScaled().y+10.0f}, std::to_string(ship->decisions.acceleration.x), ship->GetColor());
			DrawString(olc::vf2d{ ship->GetPositionScaled().x, ship->GetPositionScaled().y+20.0f}, std::to_string(ship->decisions.shootDecision), ship->GetColor());
		}
		for (const auto& projectile : gs.gg->projectiles)
		{			
			DrawRect(projectile->GetPositionScaled(), projectile->GetSizeScaled(), projectile->GetColor());
		}


		//Clear(olc::BLACK);
		//for (const auto& ship : game.ships)
		//{			
		//	DrawRect(ship->GetPositionScaled(), ship->GetSizeScaled(), ship->GetColor());
		//	DrawString(ship->GetPositionScaled(), std::to_string((int)ship->GetHealth()), ship->GetColor());			
		//}
		//for (const auto& projectile : game.projectiles)
		//{			
		//	DrawRect(projectile->GetPositionScaled(), projectile->GetSizeScaled(), projectile->GetColor());
		//}
	}

	//Game game;	
	GameSequencer gs;
};

int main()
{
	GraphicsModule gfx;
	if (gfx.Construct(fieldWidth, fieldHeight, 2, 2))
		gfx.Start();
	return 0;
}