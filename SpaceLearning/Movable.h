#pragma once
#include "olcPixelGameEngine.h"
#include "Shared.h"


struct MovableData
{
	olc::vf2d size;
	olc::vf2d position;
	olc::Pixel color;
};


class Movable
{
public:
	Movable(MovableData data, olc::vf2d startingVel = olc::vf2d{ 0,0 }) :
		size(data.size),
		position(data.position),
		color(data.color),
		velocity(startingVel)
	{}

	virtual void Update(float dt) { position += velocity * dt; }
	olc::vf2d GetSize() const { return size; }
	olc::vf2d GetPosition() const { return position; }
	olc::vf2d GetSizeScaled() const { return { size.x * fieldWidth, size.y * fieldHeight }; }
	olc::vf2d GetPositionScaled() const { return { position.x * fieldWidth, position.y * fieldHeight }; }
	olc::vf2d GetVelocity() const { return velocity; }
	olc::Pixel GetColor() const { return color; }
	void ConfineInRect(olc::vf2d dimentions);

protected:
	olc::vf2d size;
	olc::vf2d position;
	olc::vf2d velocity;
	olc::Pixel color;
};