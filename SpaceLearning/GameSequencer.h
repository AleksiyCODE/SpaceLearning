#pragma once
#include "Game.h"
#include <chrono>
#include <thread>

enum class GameResult
{
	PlyaerWon,
	EnemyWon,
	Tie,
	Timeout
};

class GameSequencer
{
public:
	
	void SequenceTraining(std::string initialNetFile = "")
	{
		ShipBrainRequieredInfo infoReference;
		std::vector<const float*>sourceData;
		FillSourceData(infoReference, sourceData);

		NeuralTrainer nt;
		bool trainingDone = false;
		if (initialNetFile == "")
		{
			nt.InitializeFirstNetBatch(sourceData, { nodesFirstLayer, nodesSecondLayer, nodesThirdLayer}, outputNodes);
		}
		else
		{
			std::ifstream myfile;
			myfile.open(initialNetFile);
			nt.SeedInitialNet(Network{sourceData, myfile });
			myfile.close();
		}

		size_t generation = 0;
		for (;!trainingDone;)
		{
			generation++;
			if (generation > generationLimit)
				trainingDone = true;
			float maxResultThisGen = 0.0f;
			std::vector<float> prfomanceData;
			for (auto& net : nt.networks)
			{
				GameResult result;
				Game g(BrainType::Simple);
				auto& trainee = g.ships[trainingNumber];
				auto& opponent = g.ships[opposingNumber];
				FillInfoReferenceConstants(&g, opponent, trainee, infoReference);

				float timeResult = 0.0f;
				float healthResult = 0.0f;
				float damageResult = 0.0f;
				for (size_t pass = 0; pass < gamePassesForEvaluation; ++pass)
				{
					float curSimulationTime = 0.0f;
					g.ResetGame();
					if (pass < gamePassesForEvaluation / 2)
					{
						g.SwapBrainType(BrainType::AFK);
					}
					else
					{
						g.SwapBrainType(BrainType::AFK);
					}
					for (;;)
					{
						curSimulationTime += simulatedFrameTime;
						if (curSimulationTime > limitSimulationTime)
						{
							result = GameResult::Timeout;
							break;
						}
						if (g.ships.size() == 0)
						{
							result = GameResult::Tie;
							break;
						}
						else if (g.ships.size() == 1)
						{
							if (g.ships[0]->GetTeam() == Team::Player)
								result = GameResult::EnemyWon;
							else
								result = GameResult::PlyaerWon;
							break;
						}
						UpdateInfoReference(&g, opponent.get(), trainee.get(), infoReference);

						net.ProcNetwork();
						
						UpdateGameState(&g, simulatedFrameTime, &net);
					}
					timeResult += 1.0f - ((1.0f / limitSimulationTime) * curSimulationTime);

					if (result == GameResult::EnemyWon || result == GameResult::Tie)
						healthResult += 0.0;
					else						
						healthResult += g.ships[0]->GetHealth() / NeuralShipData.health;		
					
					if (result == GameResult::PlyaerWon || result == GameResult::Tie)
						damageResult += 1.0f;
					else
					{
						damageResult += 1.0f - g.ships[0]->GetHealth() / OpposingShipData.health;
					}
				}
				float totalResult = (((timeImportance * timeResult) + (healthImportance * healthResult) + (damageImportance * damageResult))/((float)gamePassesForEvaluation));
				if (totalResult > maxResultThisGen)
					maxResultThisGen = totalResult;
				if (totalResult > desieredPerfomance)
				{
					std::ofstream myfile;
					myfile.open("successResult.txt");
					myfile << net.Serialize();
					myfile.close();
					trainingDone = true;
					break;
				}
				prfomanceData.push_back(totalResult + RU.GetFloat(-1.0f, 1.0f) * randomPerfomanceFluctuation);
				//prfomanceData.push_back(totalResult);
			}
			std::cout << generation << '\n';
			std::cout << maxResultThisGen << '\n';
			std::cout << "_____________________\n";
			if (generation % logEveryGen == 0)
			{
				std::ofstream myfile;
				myfile.open("complementaryResult.txt");
				myfile << nt.networks.front().Serialize();
				myfile.close();
			}
			nt.MutateNets(prfomanceData);
		}
	}

	void SequenceTesting()
	{
		float a;
		float b;
		std::vector<const float*>sourceData;
		sourceData.push_back(&a);
		sourceData.push_back(&b);

		NeuralTrainer nt;
		bool trainingDone = false;
		nt.InitializeFirstNetBatch(sourceData, { nodesFirstLayer, nodesSecondLayer, nodesThirdLayer }, 1);

		size_t generation = 0;
		for (;;)
		{
			generation++;
			if (generation > generationLimit)
				trainingDone = true;
			float maxResultThisGen = 0.0f;
			std::vector<float> prfomanceData;
			for (auto& net : nt.networks)
			{
				float totalResult = 0.0f;
				auto& result = net.layers.back()[0].output;

				a = 0;
				b = 0;
				net.ProcNetwork();
				totalResult += -(result - 1) * (result - 1) + 1;

				a = 0;
				b = 1;
				net.ProcNetwork();
				totalResult += -(result ) * (result ) + 1;

				a = 1;
				b = 0;
				net.ProcNetwork();
				totalResult += -(result ) * (result ) + 1;

				a = 0;
				b = 0;
				net.ProcNetwork();
				totalResult += -(result - 1) * (result - 1) + 1;

				std::cout << totalResult << "\n";
				prfomanceData.push_back(totalResult);
			}
			std::cout << generation << '\n';
			std::cout << "_____________________\n";
			if (trainingDone)
			{
				std::ofstream myfile;
				myfile.open("complementaryResult.txt");
				myfile << nt.networks.front().Serialize();
				myfile.close();
				break;
			}
			nt.MutateNets(prfomanceData);
		}
	}

	void BeginPlaybackNetwork(BrainType type, bool successNetwork)
	{
		std::ifstream myfile;
		if(successNetwork)
			BeginPlaybackNetwork(type, std::string("successResult.txt"));
		else
			BeginPlaybackNetwork(type, std::string("complementaryResult.txt"));
	}

	void BeginPlaybackNetwork(BrainType type, std::string fileName)
	{
		//playbackBrain = type;
		//playbackSuccess = successNetwork;

		gg = std::make_unique<Game>(type);
		FillSourceData(gInfoReference, gSourceData);

		std::ifstream myfile;
		myfile.open(fileName);

		gNet = std::make_unique<Network>(gSourceData, myfile);
		myfile.close();

		FillInfoReferenceConstants(gg.get(), gg->ships[opposingNumber], gg->ships[trainingNumber], gInfoReference);
	}

	void StepPlayback(float dt)
	{
		if (gg->ships.size() != 2)
		{
			gg->ResetGame();
			gg->SwapBrainType();
		}
		auto& trainee = gg->ships[trainingNumber];
		auto& opponent = gg->ships[opposingNumber];
		UpdateInfoReference(gg.get(), opponent.get(), trainee.get(), gInfoReference);

		gNet->ProcNetwork();

		UpdateGameState(gg.get(), simulatedFrameTime, gNet.get());
		std::this_thread::sleep_for(std::chrono::milliseconds(long(simulatedFrameTime*1000.0f)));
	}
	
	void FillSourceData(ShipBrainRequieredInfo& infoReference, std::vector<const float*>& sourceData)
	{
		sourceData.push_back(&infoReference.distanceLeftWall);
		sourceData.push_back(&infoReference.distanceRightWall);
		sourceData.push_back(&infoReference.enemyRelPosX);
		sourceData.push_back(&infoReference.enemyRelPosY);
		sourceData.push_back(&infoReference.enemyShotRelPosX);
		sourceData.push_back(&infoReference.enemyShotRelPosY);
		sourceData.push_back(&infoReference.enemyShotVelX);
		sourceData.push_back(&infoReference.enemyShotVelY);
		sourceData.push_back(&infoReference.enemySize);
		sourceData.push_back(&infoReference.enemyVelX);
		sourceData.push_back(&infoReference.velX);
		sourceData.push_back(&infoReference.remainingShotDelay);
		sourceData.push_back(&infoReference.remainingEnemyShotDelay);

		sourceData.push_back(&infoReference.maxAccX);
		sourceData.push_back(&infoReference.maxVelX);
		sourceData.push_back(&infoReference.shotVelY);
		sourceData.push_back(&infoReference.size);
	}

	void FillInfoReferenceConstants(const Game* g, std::unique_ptr<Ship>& opponent, std::unique_ptr<Ship>& player, ShipBrainRequieredInfo& infoReference)
	{
		infoReference.size = g->ships[trainingNumber]->GetSize().x;
		infoReference.enemySize = g->ships[opposingNumber]->GetSize().x;
		infoReference.maxAccX = enemyData.maxAcceleration.x;
		infoReference.maxVelX = enemyData.maxVelocity.x;
		infoReference.shotVelY = enemyData.shotSpeed;
		infoReference.enemyRelPosY = opponent->GetPosition().x - player->GetPosition().y + player->GetSize().y;
	}

	void UpdateInfoReference (Game* g, Ship* opponent, Ship* player, ShipBrainRequieredInfo& infoReference )
	{
		infoReference.distanceLeftWall = player->GetPosition().x;
		infoReference.distanceRightWall = 1.0f - (player->GetPosition().x + player->GetSize().x);
		infoReference.enemyRelPosX = (player->GetPosition().x + player->GetSize().x / 2.0f) - (opponent->GetPosition().x + opponent->GetSize().x / 2.0f);
		infoReference.enemyRelPosY = (player->GetPosition().y + player->GetSize().y) - opponent->GetPosition().y;
		if (g->importantProjectile)
		{
			infoReference.enemyShotRelPosX = g->importantProjectile->GetPosition().x - g->ships[trainingNumber]->GetPosition().x;		//WARNING! may be bad for different ship and projectile sizes
			infoReference.enemyShotRelPosY = g->importantProjectile->GetPosition().y - g->ships[trainingNumber]->GetPosition().y;
			infoReference.enemyShotVelX = g->importantProjectile->GetVelocity().x - g->ships[trainingNumber]->GetVelocity().x;
			infoReference.enemyShotVelY = g->importantProjectile->GetVelocity().y;
		}
		else
		{
			infoReference.enemyShotRelPosX = 0.0f;
			infoReference.enemyShotRelPosY = 0.0f;
			infoReference.enemyShotVelX = 0.0f;
			infoReference.enemyShotVelY = 0.0f;
		}
		infoReference.enemyVelX = g->ships[opposingNumber]->GetVelocity().x;
		infoReference.remainingEnemyShotDelay = g->ships[opposingNumber]->GetRemainingShotDelay();
		infoReference.remainingShotDelay = g->ships[trainingNumber]->GetRemainingShotDelay();
		infoReference.velX = g->ships[trainingNumber]->GetVelocity().x;
	}

	void UpdateGameState(Game* g, float dt, Network* net)
	{
		g->ships[trainingNumber]->GetBrainForUpdating().SetAcceleration(net->layers.back()[0].output * enemyData.maxAcceleration.x);	//hardcoded so that only enemyShip is being trained
		g->ships[trainingNumber]->GetBrainForUpdating().SetShotFlag(net->layers.back()[1].output);
		g->Update(dt);
	}

	   
	ShipBrainRequieredInfo gInfoReference;		//global for consecutive playbacks
	std::vector<const float*>gSourceData;
	std::unique_ptr<Network> gNet;
	std::unique_ptr<Game> gg;
	//BrainType playbackBrain;
	//bool playbackSuccess;
	private:

	static constexpr const ShipData& NeuralShipData = enemyData;
	static constexpr const ShipData& OpposingShipData = playerData;

	static constexpr size_t generationLimit = 1000;
	static constexpr size_t logEveryGen = 15;
	static constexpr size_t nodesFirstLayer = 50;
	static constexpr size_t nodesSecondLayer = 30;
	static constexpr size_t nodesThirdLayer = 15;
	static constexpr size_t outputNodes = 2;
	static constexpr size_t trainingNumber = 1;			//in array of g.ships
	static constexpr size_t opposingNumber = 0;			//in array of g.ships

	static constexpr float simulatedFrameTime = 0.03f;			//the faster the game simulates - the less percise the simulation is.
	static constexpr float limitSimulationTime = 60.0f;
	static constexpr float optimalSimulationTime = 30.0f;
	static constexpr float timeImportance = 1.0f;
	static constexpr float healthImportance = 0.5f;
	static constexpr float damageImportance = 2.0f;

	static constexpr size_t gamePassesForEvaluation = 4;			//in array of g.ships
	//static constexpr float desieredPerfomance = 4.55f;
	static constexpr float desieredPerfomance = 2.83f;
	static constexpr float randomPerfomanceFluctuation = 0.001f;
};







