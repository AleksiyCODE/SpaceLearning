#pragma once
#include <vector>
#include <tuple>
#include "Shared.h"
#include <unordered_map>
#include <algorithm>
#include <iterator>
#include <cassert>
#include "RandomUnit.h"

class Neuron
{
public:
	Neuron() = default;
	Neuron& operator=(const Neuron&) = default;		//don't do it!
	Neuron(const Neuron&) = default;				//don't do it!

	std::vector<std::pair<const float*, float>> intputsToWeights;
	float output;
	float power = 1.0f;
};

class Network
{
public:
	Network(const std::vector<const float*>& sourceData, std::ifstream& serialized)
	{
		
		std::string buf;
		float val;
		size_t layerNum;
		size_t nodeNum;
		serialized >> buf;		//{
		assert(buf == "{");
		serialized >> layerNum;		//{

		//input layer
		layers.emplace_back();
		serialized >> buf;			//	{
		serialized >> nodeNum;			//	{
		for (size_t node = 0; node < nodeNum; ++node)
		{
			serialized >> buf;		//1
			layers[0].emplace_back();
			layers[0][node].intputsToWeights.emplace_back(sourceData[node], 1.0f);
			serialized >> buf;		//1
			layers[0][node].power = 1.0f;
		}
		serialized >> buf;			//	}
		//hidden and output layers
		for (size_t layer = 1; layer < layerNum; ++layer)
		{
			serialized >> buf;		//	{
			serialized >> nodeNum;

			layers.emplace_back();
			for (size_t node = 0; node < nodeNum; ++node)
			{
				layers[layer].emplace_back();
				auto& initNode = layers[layer][node];
				for (size_t inp = 0; inp < layers[layer - 1].size(); ++inp)
				{
					serialized >> val;
					initNode.intputsToWeights.emplace_back(&layers[layer - 1][inp].output, val);		//layer - 1 -> looking at last layer
				}
				serialized >> val;
				initNode.power = val;
			}
			serialized >> buf;		//	}
		}
	}

	//create mutated copy of Network
	Network(const Network & net, float fluctPower)
	{
		//input layer
		layers.emplace_back();
		for (size_t node = 0; node < net.layers[0].size(); ++node)
		{
			layers[0].emplace_back();
			layers[0][node].intputsToWeights.emplace_back(net.layers[0][node].intputsToWeights[0].first, 1.0f);
		}

		//hidden and output layers
		for (size_t layer = 1; layer < net.layers.size(); ++layer)
		{
			layers.emplace_back();
			for (size_t node = 0; node < net.layers[layer].size(); ++node)
			{
				layers[layer].emplace_back();
				auto& initNode = layers[layer][node];
				for (size_t inp = 0; inp < layers[layer - 1].size(); ++inp)
				{
					initNode.intputsToWeights.emplace_back(&layers[layer - 1][inp].output, std::clamp(net.layers[layer][node].intputsToWeights[inp].second + RU.GetFloat(-1.0f, 1.0f) * fluctPower, -1.0f, 1.0f));
				}
				initNode.power = net.layers[layer][node].power + (RU.GetFloat(-1.0f, 1.0f) * fluctPower);
			}
		}
	}

	Network(const std::vector<const float*> & sourceData, const std::vector<size_t> & nodeNumberPerHiddenLayer, size_t outputNodes)
	{
		//genSurvived = gen;
		//input layer
		layers.emplace_back();
		for (size_t node = 0; node < sourceData.size(); ++node)
		{
			layers[0].emplace_back();
			layers[0][node].intputsToWeights.emplace_back(sourceData[node], 1.0f);
		}

		//hidden layers
		for (size_t layer = 1; layer < nodeNumberPerHiddenLayer.size() + 1; ++layer)
		{
			layers.emplace_back();
			for (size_t node = 0; node < nodeNumberPerHiddenLayer[layer - 1]; ++node)				//layer - 1 -> because started from 1
			{
				layers[layer].emplace_back();
				auto& initNode = layers[layer][node];
				for (size_t inp = 0; inp < layers[layer - 1].size(); ++inp)
				{
					initNode.intputsToWeights.emplace_back(&layers[layer - 1][inp].output, RU.GetFloat(-1.0f, 1.0f));		//layer - 1 -> looking at last layer
				}
			}
		}

		//output layer
		layers.emplace_back();
		for (size_t node = 0; node < outputNodes; ++node)
		{
			layers.back().emplace_back();
			auto& initNode = layers.back()[node];
			for (size_t inp = 0; inp < layers[layers.size() - 2].size(); ++inp)
			{
				initNode.intputsToWeights.emplace_back(&layers[layers.size() - 2][inp].output, RU.GetFloat(-1.0f, 1.0f));
			}
		}
	}

	void ProcNetwork()
	{
		for (size_t layer = 0; layer < layers.size(); ++layer)
		{
			for (size_t node = 0; node < layers[layer].size(); ++node)
			{
				float result = 0.0f;
				for (auto& inps : layers[layer][node].intputsToWeights)
				{
					result += *inps.first * inps.second;
				}
				layers[layer][node].output = tanh(result) * (layers[layer][node].power);
			}
		}
	}

	std::string Serialize() const
	{
		std::stringstream ss;
		ss << "{\n";
		ss << layers.size() << '\n';
		for (const auto& layer : layers)
		{
			ss << "	{\n";
			ss << "	" << layer.size() << '\n';
			for (const auto& neuron : layer)
			{
				for (const auto& input : neuron.intputsToWeights)
				{
					ss << "		" << input.second << '\n';
				}
				ss << "			" << neuron.power << '\n';
			}
			ss << "	}\n";
		}
		ss << "}\n";
		return ss.str();
	}
	size_t genSurvived = 0;
	std::vector<std::vector<Neuron>> layers;
};

enum class NetworkGen
{
	Previous,
	NewMinorChange,
	NewSmallChange,
	NewSignificantChange,
	NewFundamentalChange
};

class NeuralTrainer
{
public:
	void SeedInitialNet(Network net)
	{
		networks.push_back(net);
		AddMutatedNetSet(net, MinorChange, newMinorChangeNetNumber);
		AddMutatedNetSet(net, SmallChange, newSmallChangeNetNumber);
		AddMutatedNetSet(net, SignificantChange, newSignificantChangeNetNumber);
		AddMutatedNetSet(net, FundamentalChange, newFundamentalChangeNetNumber);
	}
	void InitializeFirstNetBatch(const std::vector<const float*>& sourceData, const std::vector<size_t>& nodeNumberPerHiddenLayer, size_t outputNodes)
	{
		for (size_t i = 0; i < initialNetNumber; ++i)
		{
			networks.emplace_back(sourceData, nodeNumberPerHiddenLayer, outputNodes);			
		}
	}

	//void ComputeNets()
	//{
	//	for (auto& net : networks)
	//	{
	//		net.ProcNetwork();
	//	}
	//}
#include <iostream>			//DEBUG
	void MutateNets( std::vector<float> successValues)
	{
		//removing bad nets
		std::vector<size_t> keepIndexes(carryNetNumber, 0);
		for (size_t keeping = 0; keeping < carryNetNumber; ++keeping)
		{
			auto max = std::max_element(successValues.begin(), successValues.end());
			keepIndexes[keeping] = std::distance(successValues.begin(), max);
			std::cout << *max << '\n';			//DEBUG
			*max = -999.0f;
		}
		size_t i = 0;
		for (std::list<Network>::iterator it = networks.begin(); it != networks.end();  ++i)
		{
			if (std::find(keepIndexes.begin(), keepIndexes.end(), i) == keepIndexes.end())
				it = networks.erase(it);
			else
				++it;
		}

		//mutating good nets

		size_t j = 0;
		for (std::list<Network>::iterator it = networks.begin(); j < carryNetNumber; ++it, ++j)
		{
			AddMutatedNetSet(*it, MinorChange, newMinorChangeNetNumber);
			AddMutatedNetSet(*it, SmallChange, newSmallChangeNetNumber);
			AddMutatedNetSet(*it, SignificantChange, newSignificantChangeNetNumber);
			AddMutatedNetSet(*it, FundamentalChange, newFundamentalChangeNetNumber);
		}		
	}

	 std::list<Network> networks;
private:

	void AddMutatedNetSet(Network& net, float mutationPower, size_t netNumber)
	{
		for (size_t i = 0; i < netNumber; ++i)
		{
			EmplaceMutatedNet(net, mutationPower);
		}
	}

	void EmplaceMutatedNet(Network& net, float mutationPower)
	{
		networks.emplace_back(net, mutationPower);
	}

	//static constexpr size_t initialNetNumber = 100;
	//
	//static constexpr size_t carryNetNumber = 5;
	//static constexpr size_t newMinorChangeNetNumber = 5;
	//static constexpr size_t newSmallChangeNetNumber = 5;
	//static constexpr size_t newSignificantChangeNetNumber = 5;
	//static constexpr size_t newFundamentalChangeNetNumber = 5;

#ifdef DEBUG

	static constexpr size_t initialNetNumber = 10;

	static constexpr size_t carryNetNumber = 3;
	static constexpr size_t newMinorChangeNetNumber = 2;
	static constexpr size_t newSmallChangeNetNumber = 2;
	static constexpr size_t newSignificantChangeNetNumber = 2;
	static constexpr size_t newFundamentalChangeNetNumber = 2;

#endif // DEBUG

#ifndef DEBUG
	static constexpr size_t initialNetNumber = 500;

	static constexpr size_t carryNetNumber = 10;
	static constexpr size_t newMinorChangeNetNumber = 7;
	static constexpr size_t newSmallChangeNetNumber = 4;
	static constexpr size_t newSignificantChangeNetNumber = 3;
	static constexpr size_t newFundamentalChangeNetNumber = 2;

#endif // !DEBUG

	static constexpr float MinorChange = 0.01f;
	static constexpr float SmallChange = 0.03f;
	static constexpr float FundamentalChange = 0.7f;
	static constexpr float SignificantChange = 0.1f;
};
