#pragma once
#include "Ships.h"
#include "Projectile.h"
#include <memory>
#include <vector>
#include <algorithm>
#include "Neural.h"



class Game
{
public:
	Game(BrainType opponentType)
	{
		initialBrainType = opponentType;
		ResetGame();
		SwapBrainType(opponentType);
	}
	void Update(float dt)
	{
		//Update positions
		for (auto& ship : ships)
		{
			ship->Update(dt);
		}
		for (auto& projectile : projectiles)
		{
			projectile->Update(dt);
		}

		//UpdateBrains
		

		//check collisions
		for (auto& ship : ships)
		{
			//with projectiles
			for (auto& proj : projectiles)
			{
				if (Collided(ship.get(), proj.get()))
				{
					if (proj->owner != ship->GetTeam())
					{
						ship->GetHit(proj->damage);
						proj->Hit();
					}
				}
			}
			if(importantProjectile)
				if (Collided(ship.get(), importantProjectile.get()))
				{
					if (importantProjectile->owner != ship->GetTeam())
					{
						ship->GetHit(importantProjectile->damage);
						importantProjectile->Hit();
					}
				}

			//with borders
			ship->ConfineInRect({ 1, 1 });
		}

		//remove dead objects
		projectiles.erase(std::remove_if(projectiles.begin(), projectiles.end(), [](const auto & proj) {return proj->GetAliveTimeLeft() <= 0; }), projectiles.end());
		if (importantProjectile)
			if (importantProjectile->GetAliveTimeLeft() < 0.0f)
				importantProjectile = {};
		ships.erase(std::remove_if(ships.begin(), ships.end(), [](const auto & ship) {return ship->GetHealth() <= 0; }), ships.end());
		
	}
	void AddShot(Projectile proj)
	{
		bool added = false;
		if (proj.owner == Team::Player)
		{
			if (importantProjectile)
				if (importantProjectile->GetPosition().x > proj.GetPosition().x)
				{
					projectiles.push_back(std::move(importantProjectile));
					importantProjectile = std::make_unique< Projectile>(proj);
					added = true;
				}
		}
		if(!added)
			projectiles.emplace_back(std::make_unique<Projectile>(proj));
	}

	void SwapBrainType()
	{
		auto& playerBrain = ships[0]->GetBrainForUpdating();
		auto& enemyBrain = ships[1]->GetBrainForUpdating();

		playerBrain.enemyData = ships[1].get();
		playerBrain.myData = ships[0].get();

		enemyBrain.enemyData = ships[0].get();
		enemyBrain.myData = ships[1].get();
	}

	void SwapBrainType(BrainType brain)
	{
		ships[0]->SetBrainType(brain);
		SwapBrainType();
		initialBrainType = brain;
	}
	void ResetGame()
	{
		ships.clear();
		importantProjectile.reset();
		projectiles.clear();
		ships.emplace_back(std::make_unique<PlayerShip>(MovableData{ olc::vf2d{ 0.1f, 0.06f }, olc::vf2d{ RU.GetFloat0to1(), 0.9f }, olc::BLUE }, this, initialBrainType));
		ships.emplace_back(std::make_unique<EnemyShip>(MovableData{ olc::vf2d{ 0.1f,0.06f }, olc::vf2d{ RU.GetFloat0to1(), 0.05f }, olc::RED }, this, BrainType::Neural));
	}
	BrainType initialBrainType;
	const float simulationSpeed = 1.0f;
	std::vector<std::unique_ptr<Ship>> ships;
	std::vector<std::unique_ptr<Projectile>> projectiles;
	std::unique_ptr<Projectile> importantProjectile;			//network bases decisions on this projectile (This is supposed to be the left most projectile in a burst. ShipWidth will be added to it's size later to emulate danger zone)
private :
	bool Collided(const Movable* a, const Movable* b)
	{
		auto aPos = a->GetPosition();
		auto bPos = b->GetPosition();
		auto aSize = a->GetSize();
		auto bSize = b->GetSize();

		if (aPos.x < bPos.x + bSize.x &&
			aPos.x + aSize.x > bPos.x &&
			aPos.y < bPos.y + bSize.y &&
			aSize.y + aPos.y > bPos.y)
			return true;
		else
			return false;
	}
};