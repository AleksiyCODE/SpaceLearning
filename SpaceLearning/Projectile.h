#pragma once
#include "Movable.h"
#include "Shared.h"

class Projectile : public Movable
{
public:
	Projectile(MovableData data, Team owner, float damage, olc::vf2d startingVelocity, float lifetime) :
		Movable(data, startingVelocity),
		owner(owner),
		damage(damage),
		aliveTimeLeft(lifetime)
	{}
	void Update(float dt) override
	{
		aliveTimeLeft -= dt;
		Movable::Update(dt);
	}
	void Hit()
	{
		aliveTimeLeft = 0;
	}
	static olc::vf2d DefaultSize()
	{
		return { 0.006f, 0.012f };
	}
	float GetAliveTimeLeft() const { return aliveTimeLeft; }
	const float damage;
	const Team owner;
private:
	float aliveTimeLeft = 0.0f;
};
