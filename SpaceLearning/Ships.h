#pragma once
#include "Movable.h"
#include "Shared.h"


class Game;

struct ShipData
{
	const olc::vf2d maxAcceleration;
	const olc::vf2d maxVelocity;
	float health;
	float damage;
	float shotSpeed;
	float shotLifetime;
	float shotDelay;
	const Team team;
};

struct ShipBrainDecision
{
	float shootDecision;
	bool shoot;
	olc::vf2d acceleration;
};

struct ShipBrainRequieredInfo
{
	float distanceLeftWall;
	float distanceRightWall;
	float enemyRelPosX;
	float enemyRelPosY;
	float velX;
	float enemyVelX;
	float enemyShotRelPosX;
	float enemyShotRelPosY;
	float enemyShotVelX;
	float enemyShotVelY;
	float remainingShotDelay;
	float remainingEnemyShotDelay;

	/*const*/ float shotVelY;
	/*const*/ float maxVelX;
	/*const*/ float maxAccX;
	/*const*/ float size;
	/*const*/ float enemySize;
};

const ShipData enemyData
{
	{1.0f, 0.0f},
	{0.3f, 0.3f},
	350.0f,
	60.0f,
	1.3f,
	1.9f,
	2.0f,
	Team::Enemy
};

const ShipData playerData
{
	{0.3f, 0.0f},
	{0.25f, 0.25f},
	550.0f,
	60.0f,
	1.0f,
	1.4f,
	1.5f,
	Team::Player
};

class ShipBrain
{
public:
	const Movable* myData;
	const Movable* enemyData;
	virtual ShipBrainDecision GetDirections() const = 0;

	void SetAcceleration(float x)
	{
		acceleration = std::clamp(x, -playerData.maxAcceleration.x, playerData.maxAcceleration.x);			//hardcoded so that only enemyShip is being trained		
																											//also, no clamping is needed
	}

	void SetShotFlag(float x)
	{
		shoot = x;
	}

protected:
	float acceleration = 0.0f;
	float shoot = 0.0f;
	const float shootThreshold = 0.1f;
};

class SimpleBrain : public ShipBrain
{
	ShipBrainDecision GetDirections() const override
	{
		return { 1.0f, true, enemyData->GetPosition() - myData->GetPosition() };
	}
};

class AFKBrain : public ShipBrain
{
	ShipBrainDecision GetDirections() const override
	{
		return { 1.0f, true, {0, 0} };
	}
};

class NeuralBrain : public ShipBrain
{
	ShipBrainDecision GetDirections() const override
	{
		return { shoot, shoot > shootThreshold ? true : false , {acceleration, 0.0f} };
	}
};

enum class BrainType
{
	Simple,
	Neural,
	AFK,
	None
};


class Ship : public Movable
{
public:
	Ship(MovableData movData, ShipData shipData, Game* game, BrainType type) :
		Movable(movData),
		shipData(shipData),
		game(game),
		brainType(type)
	{
		SetBrainType(type);
		shotCooldown = shipData.shotDelay;
	};
	ShipBrain& GetBrainForUpdating() { return *brain.get(); }
	void GetHit(float damage) { shipData.health -= damage; }
	float GetRemainingShotDelay() const { return shotCooldown; }
	float GetHealth() const { return shipData.health; }
	float GetMaxHealth() const { return shipData.health; }
	Team GetTeam() const { return shipData.team; }
	const ShipData& GetShipData() const {return shipData;}
	void SetBrainType(BrainType type)
	{
		switch (type)
		{
		case BrainType::None:
			brain = std::make_unique<SimpleBrain>();
			break;
		case BrainType::Simple:
			brain = std::make_unique<SimpleBrain>();
			break;
		case BrainType::Neural:
			brain = std::make_unique<NeuralBrain>();
			break;
		case BrainType::AFK:
			brain = std::make_unique<AFKBrain>();
			break;
		default:
			break;
		}
	}

	
	ShipBrainDecision decisions;
protected:
	Team team;
	void Shoot(bool upwards, bool triplicate = false);
	float shotCooldown = 0.0f;
	ShipData shipData;
	Game* game;
	BrainType brainType;
	std::unique_ptr<ShipBrain> brain;
};


class StaticKeyboard
{
public:
	static olc::HWButton GetKey(olc::Key key)
	{
		return engine->GetKey(key);
	}
	static void SetEngine(const olc::PixelGameEngine* eng)
	{
		engine = eng;
	}

private:
	static const olc::PixelGameEngine* engine;
};

class PlayerShip : public Ship
{
public:
	PlayerShip(MovableData data, Game* game, BrainType type) :
		Ship(data, playerData, game, type)
	{}
	void Update(float dt) override;
private:
};

class EnemyShip : public Ship
{
public:
	EnemyShip(MovableData data, Game* game, BrainType type) :
		Ship(data, enemyData, game, type)
	{}
	void Update(float dt) override;
private:
};