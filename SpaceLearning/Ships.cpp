#include "Ships.h"
#include "Projectile.h" 
#include "Game.h"


void PlayerShip::Update(float dt)
{
	if (brainType == BrainType::None)
	{
		if (StaticKeyboard::GetKey(olc::Key::LEFT).bHeld)
		{
			velocity -= (shipData.maxAcceleration * dt);
			velocity.x = std::max(-shipData.maxVelocity.x, velocity.x);
		}
		if (StaticKeyboard::GetKey(olc::Key::RIGHT).bHeld)
		{
			velocity += (shipData.maxAcceleration * dt);
			velocity.x = std::min(shipData.maxVelocity.x, velocity.x);
		}
		if (StaticKeyboard::GetKey(olc::Key::SPACE).bPressed)
		{
			Shoot(true);
		}
	}
	else
	{
		decisions = brain->GetDirections();
		if (decisions.shoot)
			Shoot(true, true);
		float xAcc = decisions.acceleration.x > 0 ? std::min(decisions.acceleration.x, shipData.maxAcceleration.x) : std::max(decisions.acceleration.x, -shipData.maxAcceleration.x);
		float yAcc = decisions.acceleration.y > 0 ? std::min(decisions.acceleration.y, shipData.maxAcceleration.y) : std::max(decisions.acceleration.y, -shipData.maxAcceleration.y);
		velocity += (olc::vf2d{ xAcc, yAcc } *dt);
		velocity.x = std::clamp(velocity.x, -shipData.maxVelocity.x, shipData.maxVelocity.x);
		velocity.y = std::clamp(velocity.y, -shipData.maxVelocity.y, shipData.maxVelocity.y);
	}

	if (shotCooldown > 0)shotCooldown -= dt;
	Ship::Update(dt);
}

const olc::PixelGameEngine* StaticKeyboard::engine;

void EnemyShip::Update(float dt)
{
	if (brainType == BrainType::None)
	{
		if (StaticKeyboard::GetKey(olc::Key::LEFT).bHeld)
		{
			velocity -= (shipData.maxAcceleration * dt);
			velocity.x = std::max(-shipData.maxVelocity.x, velocity.x);
		}
		if (StaticKeyboard::GetKey(olc::Key::RIGHT).bHeld)
		{
			velocity += (shipData.maxAcceleration * dt);
			velocity.x = std::min(shipData.maxVelocity.x, velocity.x);
		}
		if (StaticKeyboard::GetKey(olc::Key::SPACE).bPressed)
		{
			Shoot(false);
		}
	}
	else
	{
		decisions = brain->GetDirections();
		if (decisions.shoot)
			Shoot(false);
		float xAcc = decisions.acceleration.x > 0 ? std::min(decisions.acceleration.x, shipData.maxAcceleration.x) : std::max(decisions.acceleration.x, -shipData.maxAcceleration.x);
		float yAcc = decisions.acceleration.y > 0 ? std::min(decisions.acceleration.y, shipData.maxAcceleration.y) : std::max(decisions.acceleration.y, -shipData.maxAcceleration.y);
		//float xAcc = decisions.acceleration.x > 0 ? shipData.maxAcceleration.x : -shipData.maxAcceleration.x;
		//float yAcc = decisions.acceleration.y > 0 ? shipData.maxAcceleration.y : -shipData.maxAcceleration.y;
		velocity += (olc::vf2d{ xAcc, yAcc } *dt);
		velocity.x = std::clamp(velocity.x, -shipData.maxVelocity.x, shipData.maxVelocity.x);
		velocity.y = std::clamp(velocity.y, -shipData.maxVelocity.y, shipData.maxVelocity.y);
	}

	if (shotCooldown > 0)shotCooldown -= dt;
	Ship::Update(dt);
}

void Ship::Shoot(bool upwards, bool triplicate)
{
	if (shotCooldown <= 0.0f)
	{
		if (upwards)
		{
			game->AddShot(Projectile{
				MovableData{Projectile::DefaultSize(),
				{position.x + size.x/2, position.y},
				color },
				shipData.team,
				shipData.damage,
				{velocity.x, -shipData.shotSpeed},
				shipData.shotLifetime
				});
		}
		else
		{
			game->AddShot(Projectile{
				MovableData{Projectile::DefaultSize(),
				{position.x + size.x/2, position.y + size.y},
				color },
				shipData.team,
				shipData.damage,
				{velocity.x, shipData.shotSpeed},
				shipData.shotLifetime
				});
		}
		if (triplicate)
		{

			if (upwards)
			{
				game->AddShot(Projectile{
					MovableData{Projectile::DefaultSize(),
					{position.x, position.y},
					color },
					shipData.team,
					shipData.damage,
					{velocity.x, -shipData.shotSpeed},
					shipData.shotLifetime
					});
				game->AddShot(Projectile{
					MovableData{Projectile::DefaultSize(),
					{position.x + size.x, position.y},
					color },
					shipData.team,
					shipData.damage,
					{velocity.x, -shipData.shotSpeed},
					shipData.shotLifetime
					});
			}
			else
			{
				game->AddShot(Projectile{
					MovableData{Projectile::DefaultSize(),
					{position.x, position.y + size.y},
					color },
					shipData.team,
					shipData.damage,
					{velocity.x, shipData.shotSpeed},
					shipData.shotLifetime
					});
				game->AddShot(Projectile{
					MovableData{Projectile::DefaultSize(),
					{position.x + size.x, position.y + size.y},
					color },
					shipData.team,
					shipData.damage,
					{velocity.x, shipData.shotSpeed},
					shipData.shotLifetime
					});
			}
		}
		shotCooldown = shipData.shotDelay;
	}
}
